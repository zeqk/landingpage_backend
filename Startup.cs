﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Toolkit.Services.LinkedIn;

namespace landingpage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            
            
        }

        public async Task test() 
        {
            // Initialize service - use overload if you need to supply additional permissions
            LinkedInService.Instance.Initialize("ClientId.Text", "ClientSecret.Text", "CallbackUri.Text", null, null, null);

            // Login to LinkedIn
            if (!await LinkedInService.Instance.LoginAsync())
            {
                return;
            }

            // Get current user's profile details
            await LinkedInService.Instance.GetUserProfileAsync();

            // Share message to LinkedIn (text should include a Url so LinkedIn can scrape preview information)
            //await LinkedInService.Instance.ShareActivityAsync(ShareText.Text);
        }
    }
}
